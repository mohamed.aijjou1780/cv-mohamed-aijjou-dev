FROM ubuntu 
RUN apt-get update
RUN apt-get install nginx -y
WORKDIR /public/
COPY ..
EXPOSE 80
