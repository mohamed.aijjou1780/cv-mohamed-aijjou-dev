# cv-mohamed-aijjou-dev

## Description

CV en HTML/CSS sans Framework sous contrainte en partant d'un modèle découvert sur Pinterest.

Deployé sur Gitlab Pages : https://mohamed.aijjou1780.gitlab.io/cv-mohamed-aijjou-dev (forcez le certificat de sécurité 🌪️ )

## Detail

![forthebadge](https://forthebadge.com/images/badges/uses-html.svg) ![forthebadge](https://forthebadge.com/images/badges/uses-css.svg)


![](cv-aijjou-mohamed.png)<div>&nbsp;</div>![](copie-cv.jpg)

